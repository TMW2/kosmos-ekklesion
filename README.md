# Kosmos Ekklesion – Κόσμος (τῶν) ἐκκλησιῶν

Concept for a multi-user online role play game (MORPG) game that lets the player experience the world of the early church congregations (home churches) of the New Testament - their history, stories and beliefs.

The main idea is to experience stories (quests/adventures) regarding that time. Of course scripture plays an important role when getting to know the people of these times. It might happen that the stories of scripture have the power to teleport one into the time that is written about…


## Developer ressources

### iOS

- How to build a SDL iOS app: https://blog.wasin.io/2018/10/19/build-sdl2-application-on-ios.html
- Build OpenSSL and cURL for iOS: https://github.com/jasonacox/Build-OpenSSL-cURL
- Build libpng for iOS: https://github.com/ashtons/libtiff-ios
- Build zlib for iOS: https://github.com/leico/zlib-xcode